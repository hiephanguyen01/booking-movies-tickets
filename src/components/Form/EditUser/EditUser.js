import React, { useEffect } from "react";
import { Select } from "antd";
import { useFormik } from "formik";
import { GROUPID } from "settings/config";
import { useDispatch, useSelector } from "react-redux";
import {
  capNhatThongTinNguoiDungAction,
  layDanhSachLoaiNguoiDungAction,
  themNguoiDungAction,
} from "store/actions/QuanLyNguoiDungAction";
import "./EditUser.scss";

export const EditUser = ({ user }) => {
  const { listLoaiNguoiDung } = useSelector(
    (state) => state.quanLyNguoiDungReducer
  );
  console.log(user);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(layDanhSachLoaiNguoiDungAction());
  }, [user]);
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      taiKhoan: user.taiKhoan,
      matKhau: user.matKhau,
      email: user.email,
      soDt: user.soDt,
      maNhom: GROUPID,
      maLoaiNguoiDung: user.maLoaiNguoiDung,
      hoTen: user.hoTen,
    },
    onSubmit: (values, { resetForm }) => {
      console.log(values);
      dispatch(capNhatThongTinNguoiDungAction(values));
    },
  });

  const handleChange = (value) => {
    formik.setFieldValue("maLoaiNguoiDung", value);
  };
  // const handleReset = (resetForm) => {
  //   if (window.confirm("Reset?")) {
  //     resetForm();
  //   }
  // };
  return (
    <div className="editUser">
      <div className="newContainer">
        <div className="top">
          <h1>Edit User</h1>
        </div>
        <div className="bottom">
          <form onSubmit={formik.handleSubmit}>
            <div className="box">
              <div className="formInput">
                <label>Username</label>
                <input
                  type="text"
                  name="taiKhoan"
                  onChange={formik.handleChange}
                  value={formik.values.taiKhoan}
                  placeholder="john_doe"
                />
              </div>
              <div className="formInput">
                <label>Name and surname</label>
                <input
                  type="text"
                  name="hoTen"
                  onChange={formik.handleChange}
                  value={formik.values.hoTen}
                  placeholder="John Doe"
                />
              </div>
              <div className="formInput">
                <label>Email</label>
                <input
                  type="email"
                  name="email"
                  onChange={formik.handleChange}
                  value={formik.values.email}
                  placeholder="john_doe@gmail.com"
                />
              </div>
              <div className="formInput">
                <label>Password</label>
                <input
                  name="matKhau"
                  onChange={formik.handleChange}
                  value={formik.values.matKhau}
                  type="password"
                />
              </div>
              <div className="formInput">
                <label>Phone</label>
                <input
                  type="text"
                  name="soDt"
                  onChange={formik.handleChange}
                  value={formik.values.soDt}
                  placeholder="+84 123 12343"
                />
              </div>
              <div className="formInput">
                <label>User Type</label>
                <Select
                  className="selectuser"
                  value={formik.values.maLoaiNguoiDung}
                  onChange={handleChange}
                  options={listLoaiNguoiDung?.map((loai, idx) => ({
                    label: loai.tenLoai,
                    value: loai.maLoaiNguoiDung,
                  }))}
                ></Select>
              </div>
            </div>
            <div className="button">
              <button type="submit">Send</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
