import React from "react";
import "./footer.scss";

export const Footer = () => {
  return (
    <section className="footer" id="footer">
      <div className="box-container-footer container">
        <div className="box">
          <h3>quick links</h3>
          <a href="#home">
            <i className="fas fa-angle-right" /> home
          </a>
          <a href="#about">
            <i className="fas fa-angle-right" /> about
          </a>
          <a href="#courses">
            <i className="fas fa-angle-right" /> courses
          </a>
          <a href="#pricing">
            <i className="fas fa-angle-right" /> pricing
          </a>
          <a href="#team">
            <i className="fas fa-angle-right" /> team
          </a>
          <a href="#blogs">
            <i className="fas fa-angle-right" /> blogs
          </a>
        </div>
        <div className="box">
          <h3>contact info</h3>
          <a href="#">
            <i className="fas fa-phone" /> +123-456-7890
          </a>
          <a href="#">
            <i className="fas fa-phone" /> +111-222-3333
          </a>
          <a href="#">
            <i className="fas fa-envelope" /> shaikhanas@gmail.com
          </a>
          <a href="#">
            <i className="fas fa-map" /> mumbai, india - 400104
          </a>
        </div>
        <div className="box">
          <h3>follow us</h3>
          <a href="#">
            <i className="fab fa-facebook-f" /> facebook
          </a>
          <a href="#">
            <i className="fab fa-twitter" /> twitter
          </a>
          <a href="#">
            <i className="fab fa-instagram" /> instagram
          </a>
          <a href="#">
            <i className="fab fa-linkedin" /> linkedin
          </a>
        </div>
        <div className="box">
          <h3>newsletter</h3>
          <p>subscribe for latest updates</p>
          <form>
            <input
              type="email"
              name="email"
              className="email"
              placeholder="enter yout email"
            />
            <input type="submit" className="link-btn" value="SEND" />
          </form>
        </div>
      </div>
      <p className="credit">
        created by <span>hiephanguyen</span> | all rights reserved!
      </p>
    </section>
  );
};
