import React, { memo, useState } from "react";
import "./header.scss";
import { SearchOutlined } from "@ant-design/icons";
import { Link, NavLink, Outlet, useNavigate } from "react-router-dom";
import Tippy from "@tippyjs/react/headless"; // different import path!
import {
  LeftOutlined,
  DownOutlined,
  CaretDownOutlined,
  UserOutlined,
  LogoutOutlined,
  DashboardOutlined,
} from "@ant-design/icons";
import Box from "@mui/material/Box";

import { TOKEN, USER_LOGIN } from "settings/config";
import { useSelector } from "react-redux";
import { MenuHeader } from "components/Menu/Header";

export const Header = (props) => {
  const { navColor } = props;
  const { userLogin } = useSelector((state) => state.quanLyNguoiDungReducer);
  const navigate = useNavigate();
  return (
    <>
      <header className={navColor ? "active" : ""}>
        <a href="#" className="logo">
          <span>NHH</span>
        </a>
        <ul className="navigation">
          <li>
            <NavLink className="nav-link" to="/">
              HOME
            </NavLink>
          </li>
          <li>
            <NavLink
              style={{ pointerEvents: "none" }}
              className="nav-link"
              to="/coming"
            >
              COMING NEXT
            </NavLink>
          </li>
          <li>
            <NavLink className="nav-link" to="/movies">
              MOVIES
            </NavLink>
          </li>
          <li>
            <NavLink
              style={{ pointerEvents: "none" }}
              className="nav-link"
              to="/events"
            >
              EVENT
            </NavLink>
          </li>
          <li>
            <NavLink
              style={{ pointerEvents: "none" }}
              className="nav-link"
              to="/gift"
            >
              GIFTS & PROMOTIONS
            </NavLink>
          </li>
          <li>
            <NavLink
              style={{ pointerEvents: "none" }}
              className="nav-link"
              to="/contact"
            >
              CONTACT
            </NavLink>
          </li>
        </ul>

        <div className="search">
          {/* <SearchOutlined className="icon" /> */}
          <div className="language">
            <span className="active">EN</span>
            <span>|</span>
            <span>VN</span>
          </div>
          {localStorage.getItem(USER_LOGIN) ? (
            <MenuHeader/>
          ) : (
            <div className="button">
              <Link to="login">Login</Link>
              <Link to="register">register</Link>
            </div>
          )}
        </div>
      </header>
    </>
  );
};
