import React, { useEffect, useState,memo } from "react";
import "./DefaultLayout.scss";
import { Footer } from "components/Layouts/DefaultLayout/footer/Footer";
import { Header } from "components/Layouts/DefaultLayout/header/Header";

 const DefaultLayout = ({ children }) => {
  const [navColor, setNavColor] = useState(false);
  const listenScrollEvent = () => {
    window.scrollY > 10 ? setNavColor(true) : setNavColor(false);
  };
  useEffect(() => {
    window.addEventListener("scroll", listenScrollEvent);
    return () => {
      window.removeEventListener("scroll", listenScrollEvent);
    };
  }, []);
  return (
    <div className="wrapper">
      <Header navColor={navColor} />
      <div className="content"> {children}</div>
      <Footer />
    </div>
  );
};
export default  memo(DefaultLayout);