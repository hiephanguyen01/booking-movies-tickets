import React from "react";
import "./navbar.scss";
import DarkModeOutlinedIcon from "@mui/icons-material/DarkModeOutlined";

import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { TOKEN, USER_LOGIN } from "settings/config";
import Tippy from "@tippyjs/react/headless"; // different import path!
import {
  CaretDownOutlined,
  UserOutlined,
  LogoutOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import Box from "@mui/material/Box";

export const Navbar = () => {
  const navigate = useNavigate();
  const { userLogin } = useSelector((state) => state.quanLyNguoiDungReducer);
  return (
    <div className="navbar">
      <div className="wrapper">
        {/* <div className="search">
          <input type="text" placeholder="search..." />
          <SearchOutlinedIcon className="icon" />
        </div> */}
        <div className="items">
          {/* <div className="item">
            <LanguageOutlinedIcon className="icon" />
            <span>English</span>
          </div> */}
          <div
            className="item"
            style={{ cursor: "pointer" }}
            // onClick={()=> dispatch({type:TOGGLE})}
          >
            <DarkModeOutlinedIcon className="icon" />
          </div>

          <div className="item">
            {localStorage.getItem(USER_LOGIN) ? (
              <Tippy
                delay={[0, 500]}
                interactive
                render={(attrs) => (
                  <div className="menu-list" tabIndex="-1" {...attrs}>
                    <div className="wrapper-menu">
                      <Link
                        style={{ pointerEvents: "none" }}
                        className="menu-item"
                        to="/profile"
                      >
                        <UserOutlined className="icon" />
                        <span> My profile</span>
                      </Link>

                      <Link className="menu-item" to="/">
                        <HomeOutlined className="icon" />
                        <span> Home</span>
                      </Link>

                      <a
                        onClick={() => {
                          localStorage.removeItem(USER_LOGIN);
                          localStorage.removeItem(TOKEN);
                          navigate("/");
                        }}
                        className="menu-item separate"
                      >
                        <LogoutOutlined className="icon" />
                        <span> Log out</span>
                      </a>
                    </div>
                  </div>
                )}
              >
                <Box className="myBox" sx={{ position: "relative" }}>
                  <button className="mybutton" type="button">
                    <div className="user">
                      <span>hi, {userLogin?.hoTen}</span>
                      <div className="imgage">
                        <img
                          src="https://cf.shopee.vn/file/96fdcd65ef075b128f36c289a62723db_tn"
                          alt="dsa"
                        />
                        <CaretDownOutlined className="icon" />
                      </div>
                    </div>
                  </button>
                </Box>
              </Tippy>
            ) : (
              <div className="button">
                <Link to="login">Login</Link>
                <Link to="register">register</Link>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
