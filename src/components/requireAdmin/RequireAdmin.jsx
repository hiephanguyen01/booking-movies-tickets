import { useSelector } from "react-redux";
import { USER_LOGIN } from "settings/config";
import { openNotification } from "utils/Notification";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

const { Navigate } = require("react-router");

export const RequireAdmin = (props) => {
  const navigate = useNavigate();
  const { userLogin } = useSelector((state) => state.quanLyNguoiDungReducer);
  

  useEffect(() => {
    if (userLogin.maLoaiNguoiDung != "QuanTri") {
      console.log("dsadsa");
      openNotification("error", "Bạn không có quyền truy cập vào trang này !");
      // alert("Bạn không có quyền truy cập vào trang này !");
      navigate("/");
    }
    if (!localStorage.getItem(USER_LOGIN)) {
      navigate("/");
    }
  }, []);

  return <>{props.children}</>;
};
