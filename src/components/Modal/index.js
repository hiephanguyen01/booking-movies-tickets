import React from "react";
import { Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { HIDE_MODAL } from "store/types/modalTypes";
import "./Modal.scss";
import { EditUser } from "components/Form/EditUser/EditUser";

export const ModalLichChieu = () => {
  const { visible, Component } = useSelector((state) => state.modalReducer);
  const dispatch = useDispatch();

  return (
    <Modal
      // title={"sa"}
      closable={false}
      onCancel={() => dispatch({ type: HIDE_MODAL })}
      visible={visible}
      footer={null}
      // style={{width:"atuto"}}
      className="ModalShowtime"
    >
      {Component}
      {/* <EditUser /> */}
    </Modal>
  );
};
