import { LichChieu } from "components/lichchieu/LichChieu";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Outlet, useNavigate } from "react-router";
import {
  layDanhSachPhongVeAction,
  layThongTinLichChieuPhimAction,
} from "store/actions/QuanLyPhimAction";
import { SHOW_MODAL } from "store/types/modalTypes";
import "./film.scss";

export default function Film({ phim }) {
  const { movieDetail } = useSelector((state) => state.quanLyPhimReducer);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const layDanhSachPhongVe = async (maPhim) => {
    await dispatch(layThongTinLichChieuPhimAction(maPhim));
    dispatch({
      type: SHOW_MODAL,
      Component: <LichChieu />,
    });
  };
  return (
    <div className="film">
      <div className="image">
        <img
          src={phim?.hinhAnh}
          onError={(e) => {
            e.target.onerror = null;
            e.target.src = "https://picsum.photos/75/75";
          }}
          alt={phim?.biDanh}
        />
      </div>
      <div className="content">
        <h3>{phim?.tenPhim}</h3>
        <div className="link">
          <a
            onClick={() => {
              navigate(`/movie-detail/${phim.maPhim}`);
            }}
            className="link-btn"
          >
            Movie detail
          </a>
          <a
            onClick={() => layDanhSachPhongVe(phim?.maPhim)}
            className="link-btn"
          >
            buy sticker
          </a>
        </div>
      </div>
    </div>
  );
}
