import React from "react";
import "./MenuHeader.scss";
import { Link, useNavigate } from "react-router-dom";
import { TOKEN, USER_LOGIN } from "settings/config";
import Tippy from "@tippyjs/react/headless"; // different import path!
import {
  CaretDownOutlined,
  UserOutlined,
  LogoutOutlined,
  DashboardOutlined,
} from "@ant-design/icons";
import Box from "@mui/material/Box";
import { useSelector } from "react-redux";

export const MenuHeader = () => {
  const navigate = useNavigate();
  const { userLogin } = useSelector((state) => state.quanLyNguoiDungReducer);
  return (
    <Tippy
    delay={[0,700]}
      interactive
      render={(attrs) => (
        <div className="menu-list" tabIndex="-1" {...attrs}>
          <div className="wrapper-menu">
            <Link
              style={{ pointerEvents: "none" }}
              className="menu-item"
              to="/profile"
            >
              <UserOutlined className="icon" />
              <span> My profile</span>
            </Link>
            {userLogin.maLoaiNguoiDung == "QuanTri" && (
              <Link className="menu-item" to="/admin">
                <DashboardOutlined className="icon" />
                <span> Dashboard</span>
              </Link>
            )}
            <a
              onClick={() => {
                localStorage.removeItem(USER_LOGIN);
                localStorage.removeItem(TOKEN);
                navigate("/");
              }}
              className="menu-item separate"
            >
              <LogoutOutlined className="icon" />
              <span> Log out</span>
            </a>
          </div>
        </div>
      )}
    >
      <Box className="myBox" sx={{ position: "relative" }}>
        <button className="mybutton" type="button">
          <div className="user">
            <span>hi, {userLogin?.hoTen}</span>
            <div className="imgage">
              <img
                src="https://cf.shopee.vn/file/96fdcd65ef075b128f36c289a62723db_tn"
                alt="dsa"
              />
              <CaretDownOutlined className="icon" />
            </div>
          </div>
        </button>
      </Box>
    </Tippy>
  );
};
