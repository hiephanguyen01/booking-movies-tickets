import React from "react";
import { useSelector } from "react-redux";
import "./ResultBooking.scss";
import moment from "moment";
import _ from "lodash";

export const ResultBooking = () => {
  const { thongTinNguoiDung } = useSelector(
    (state) => state.quanLyNguoiDungReducer
  );
  return (
    <div className="resultbooking">
      {thongTinNguoiDung?.thongTinDatVe?.map((ve, idx) => {
        const seats = ve.danhSachGhe;
        return (
          <a
            key={ve.maVe}
            href="#"
            className="flex flex-col items-center bg-white rounded-lg border shadow-md md:flex-row md:max-w-xl hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700"
          >
            <img
              className="object-cover w-full h-96 rounded-t-lg md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
              src={ve.hinhAnh}
              alt={ve.tenPhim}
            />
            <div className="flex flex-col justify-between p-4 leading-normal">
              <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                {ve.tenPhim}
              </h5>
              <div className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                <p className="text-gray-500">
                  <span className="font-bold">Địa Điểm:</span>
                  {seats.tenRap}
                </p>
              </div>
              <div className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                <p className="text-gray-500">
                  <span className="font-bold">Giờ chiếu:</span>{" "}
                  {moment(ve.ngayDat).format("hh:mm A")} <br />
                  <span className="font-bold">Ngày chiếu:</span>{" "}
                  {moment(ve.ngayDat).format("DD-MM-YYYY")} .
                </p>
              </div>
              <div className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                <p className="text-gray-500">
                  <span className="font-bold ml-1">Ghế:</span>
                  {_.sortBy(seats, ["tenGhe"]).map((ghe, idx) => {
                    return <span key={ghe.maGhe}> {`[${ghe.tenGhe}]`}</span>;
                  })}
                </p>
              </div>
            </div>
          </a>
        );
      })}
    </div>
  );
};
