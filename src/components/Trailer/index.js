import React from "react";
import ReactPlayer from "react-player/lazy";
import { useSelector } from "react-redux";

export const Trailer = ({ url }) => {
  const { playing } = useSelector((state) => state.modalReducer);
  return <ReactPlayer controls={true} playing={playing} url={url} />;
};
