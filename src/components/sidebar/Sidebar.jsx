import React from "react";
import "./sidebar.scss";
import "./sidebar.scss";
import DashboardIcon from "@mui/icons-material/Dashboard";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import { MovieOutlined } from "@mui/icons-material";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
// import { DARK, LIGHT } from "store/reducer/type";

export const Sidebar = () => {
  const dispatch = useDispatch();
  return (
    <div className="sidebar">
      <div className="top">
        <Link to="/" style={{ textDecoration: "none" }}>
          <span className="logo">Admin</span>
        </Link>
      </div>
      <hr />
      <div className="center">
        <ul>
          <p className="title">MAIN</p>
          <li>
            <DashboardIcon className="icon" />
            <span>Dashboard</span>
          </li>
          <p className="title">LIST</p>
          <Link to="users" style={{ textDecoration: "none" }}>
            <li>
              <PersonOutlineIcon className="icon" />
              <span>User</span>
            </li>
          </Link>
          <Link to="films" style={{ textDecoration: "none" }}>
            <li>
              <MovieOutlined className="icon" />
              <span>Film</span>
            </li>
          </Link>
        </ul>
      </div>
      <div className="bottom">
        <div
          className="colorOption"
          // onClick={()=>dispatch({type:LIGHT})}
        ></div>
        <div
          className="colorOption"
          //  onClick={()=>dispatch({type:DARK})}
        ></div>
      </div>
    </div>
  );
};
