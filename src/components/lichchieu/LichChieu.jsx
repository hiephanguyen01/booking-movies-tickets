import React from "react";
import "./lichchieu.scss";
import { Tabs, Radio, Space } from "antd";
import moment from "moment";
import { Outlet, useNavigate } from "react-router";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { HIDE_MODAL } from "store/types/modalTypes";

const { TabPane } = Tabs;

export const LichChieu = () => {
  const dispatch = useDispatch();
  const { movieDetail } = useSelector((state) => state.quanLyPhimReducer);
  return (
    <div className="infoLichChieu">
      <h1 className="title">Lịch Chiếu</h1>
      <div className="lichchieu">
        <Tabs tabPosition="left">
          {movieDetail?.heThongRapChieu?.map((heThongRap, idx) => {
            return (
              <TabPane
                key={heThongRap.maHeThongRap}
                tab={
                  <div className="heThongRap">
                    <img src={heThongRap?.logo} alt={heThongRap.maHeThongRap} />
                  </div>
                }
              >
                {heThongRap.cumRapChieu.map((cumRap, idx) => {
                  return (
                    <div className="cumRap" key={idx}>
                      <div className="tenCum">{cumRap.tenCumRap}</div>
                      <ul className="listSeat">
                        {cumRap.lichChieuPhim.map((lichChieu, idx) => {
                          return (
                            <li key={idx}>
                              <Link
                                onClick={() => dispatch({ type: HIDE_MODAL })}
                                to={`/checkout/${lichChieu.maLichChieu}`}
                              >
                                {moment(lichChieu.ngayChieuGioChieu).format(
                                  "lll"
                                )}
                              </Link>
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  );
                })}
              </TabPane>
            );
          })}
        </Tabs>
      </div>
    </div>
  );
};
