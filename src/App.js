import { Loader } from "components/loader/Loader";
import { RequireAdmin } from "components/requireAdmin/RequireAdmin";
import DefaultLayout from "components/Layouts/DefaultLayout";
import { productInputs, userInputs } from "formSource";
import { EditFilm } from "pages/admin/films/editFilm/EditFilm";
import { Films } from "pages/admin/films/Films";
import { NewFilm } from "pages/admin/films/newfilm/NewFilm";
import { ShowTime } from "pages/admin/films/showtime/ShowTime";
import HomeAdmin from "pages/admin/home/HomeAdmin";
import New from "pages/admin/users/new/New";
import { Users } from "pages/admin/users/Users";
import { CheckOut } from "pages/client/checkOut/CheckOut";
import { MovieDetail } from "pages/client/detail/MovieDetail";
import Home from "pages/client/home/Home";
import { Login } from "pages/client/Login/Login";
import { Register } from "pages/client/Register/Register";
import { render } from "react-dom";
import { useSelector } from "react-redux";
import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
import { ModalLichChieu } from "components/Modal";
import { Movies } from "pages/client/Movies";
function App() {
  const { loader } = useSelector((state) => state.loaderReducer);
  return (
    <BrowserRouter>
      <div className="App">
        {loader ? <Loader /> : ""}
        <ModalLichChieu />

        <Routes>
          <Route
            path="/"
            element={
              <DefaultLayout>
                <Outlet />
              </DefaultLayout>
            }
          >
            <Route index element={<Home />} />
            <Route path="movie-detail/:idPhim" element={<MovieDetail />} />
            <Route path="movies" element={<Movies />} />
          </Route>

          <Route
            path="checkout/:maLichChieu"
            element={<CheckOut />}
          />
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
          <Route
            path="admin"
            element={
              <RequireAdmin>
                <HomeAdmin />
              </RequireAdmin>
            }
          >
            <Route path="users">
              <Route index element={<Users />} />
              <Route
                path="new"
                element={<New inputs={userInputs} title="add new user" />}
              />
            </Route>
            <Route path="films">
              <Route index element={<Films />} />
              <Route path="new" element={<NewFilm />} />
              <Route path="edit/:idPhim" element={<EditFilm />} />
              <Route path="showtime/:idPhim" element={<ShowTime />} />
            </Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
