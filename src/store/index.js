import { applyMiddleware,combineReducers , createStore} from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { quanLyPhimReducer } from "./reducers/QuanLyPhimReducer";
import { quanLyDatVeReducer } from "./reducers/QuanLyDatVeReducer";
import { loaderReducer } from "./reducers/LoaderReducer";
import { quanLyNguoiDungReducer } from "./reducers/QuanLyNguoiDungReducer";
import { tabReducer } from "pages/client/checkOut/modules/reducers";
import { quanLyRapReducer } from "./reducers/QuanLyRapReducer";
import { modalReducer } from "./reducers/ModalReducer";

const rootReducer = combineReducers({
  quanLyPhimReducer,
  quanLyDatVeReducer,
  loaderReducer,
  quanLyNguoiDungReducer,
  tabReducer,
  quanLyRapReducer,
  modalReducer,

});
const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

export default store;
