import { quanLyRapService } from "services/QuanLyRapService"
import { LIST_CUM_RAP, LIST_RAP } from "store/types/quanLyRapType"

export const layThongTinHeTHongRapAction =(mahethongrap="")=>{
    return async dispatch =>{
        try {
            const {data} =await quanLyRapService.layThongTinHeThongRap(mahethongrap)
            console.log(data)
            dispatch({type:LIST_RAP,listRap:data.content})
        } catch (error) {
            console.log(error.response?.data)
        }
    }
}

export const layThongTinCumRapTheoHeThongAction =(maHTR)=>{
    return async dispatch =>{
        try {
            const {data} =await quanLyRapService.layThongTinCumRapTheoHeThong(maHTR)
            console.log(data)
            dispatch({type:LIST_CUM_RAP,listCumRap:data.content})
        } catch (error) {
            console.log(error.response?.data)
        }
    }
}