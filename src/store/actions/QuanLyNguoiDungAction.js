import { quanLyNguoiDungSevice } from "services/QuanLyNguoiDung";
import { HIDE_LOADING, SHOW_LOADING } from "store/types/loaderTypes";
import { HIDE_MODAL } from "store/types/modalTypes";
import {
  DANG_NHAP,
  LIST_LOAI_NGUOI_DUNG,
  SET_LIST_USERS,
  THONG_TIN_NGUOI_DUNG,
} from "store/types/quanLyNguoiDungTypes";
import { openNotification } from "utils/Notification";

export const dangNhapAction = (infoLogin, navigation) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SHOW_LOADING });
      const { data } = await quanLyNguoiDungSevice.dangNhap(infoLogin);
      dispatch({ type: HIDE_LOADING });
      navigation(-1);
      openNotification("success", "Login success!");
      dispatch({
        type: DANG_NHAP,
        thongTinDangNhap: data.content,
      });
      dispatch({ type: HIDE_LOADING });
    } catch (error) {
      openNotification("error", `${error.response?.data.content}`);
      dispatch({ type: HIDE_LOADING });
      console.log(error.response?.data);
    }
  };
};

export const dangKyAction = (infoLogin, navigation) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SHOW_LOADING });
      await quanLyNguoiDungSevice.dangKy(infoLogin);
      dispatch({ type: HIDE_LOADING });
      navigation("/login");
      openNotification("success", "Register success!");
      dispatch({ type: HIDE_LOADING });
    } catch (error) {
      openNotification("error", `${error.response?.data.content}`);
      dispatch({ type: HIDE_LOADING });
      console.log(error.response?.data);
    }
  };
};

export const thongTinTaiKhoanAction = () => {
  return async (dispatch) => {
    try {
      const { data } = await quanLyNguoiDungSevice.thongTinTaiKhoan();
      // console.log(data);
      dispatch({
        type: THONG_TIN_NGUOI_DUNG,
        thongTinNguoiDung: data.content,
      });
    } catch (error) {
      console.log(error.response?.data);
    }
  };
};
export const layDanhSachNguoiDungAction = (tuKhoa = "") => {
  console.log(tuKhoa)
  return async (dispatch) => {
    try {
      const { data } = await quanLyNguoiDungSevice.layDanhSachNguoiDung(tuKhoa);
      // console.log(data);
      dispatch({
        type: SET_LIST_USERS,
        listUsers: data.content,
      });
    } catch (error) {
      console.log(error.response?.data);
    }
  };
};
export const layDanhSachLoaiNguoiDungAction = () => {
  return async (dispatch) => {
    try {
      const { data } = await quanLyNguoiDungSevice.layDanhSachLoaiNguoiDung();
      console.log(data);
      dispatch({
        type: LIST_LOAI_NGUOI_DUNG,
        listLoaiNguoiDung: data.content,
      });
    } catch (error) {
      console.log(error.response?.data);
    }
  };
};

export const themNguoiDungAction = (info, resetForm) => {
  return async (dispatch) => {
    try {
      await quanLyNguoiDungSevice.themNguoiDung(info);
      // console.log(data);
      openNotification("success", "Add user success!");
      resetForm();
      dispatch(layDanhSachLoaiNguoiDungAction());
    } catch (error) {
      openNotification("error", `${error.response?.data.content}`);
      console.log(error.response?.data);
    }
  };
};

export const capNhatThongTinNguoiDungAction = (user) => {
  return async (dispatch) => {
    try {
      const { data } = await quanLyNguoiDungSevice.capNhatThongTinNguoiDung(
        user
      );
      console.log(data);
      dispatch(layDanhSachNguoiDungAction());
      openNotification("success", "Update user success!");
      dispatch({type:HIDE_MODAL})
    } catch (error) {
      openNotification("error", `${error.response?.data.content}`);
      console.log(error.response?.data);
    }
  };
};
export const deleteNguoiDungAction = (taikhoan) => {
  return async (dispatch) => {
    try {
      const { data } = await quanLyNguoiDungSevice.deleteNguoiDung(taikhoan);
      console.log(data);
      dispatch(layDanhSachNguoiDungAction());
      openNotification("success", "delete user success!");
    } catch (error) {
      openNotification("error", `${error.response?.data.content}`);
      console.log(error.response?.data);
    }
  };
};
// export const timKiemNguoiDungAction = (ten) => {
//   return async (dispatch) => {
//     try {
//       const { data } = await quanLyNguoiDungSevice.timKiemNguoiDung(ten);
//       console.log(data);
//       dispatch(layDanhSachNguoiDungAction());
//       openNotification("success", "delete user success!");
//     } catch (error) {
//       openNotification("error", `${error.response?.data.content}`);
//       console.log(error.response?.data);
//     }
//   };
// };
