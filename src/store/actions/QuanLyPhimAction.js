import { HIDE_LOADING, SHOW_LOADING } from "store/types/loaderTypes";
import { openNotification } from "utils/Notification";

const { quanLyPhimService } = require("services/QuanLyPhimService");
const {
  LIST_BANNER,
  LIST_PHIM,
  INFO_PHIM,
  SET_MOVIE_DETAIL,
} = require("store/types/quanLyPhimTypes");

export const layDanhSachBannerAction = () => {
  return async (dispatch) => {
    try {
      const { data } = await quanLyPhimService.layDanhSachBanner();
      dispatch({ type: LIST_BANNER, listBanner: data.content });
    } catch (error) {
      console.log(error.response?.data);
    }
  };
};

export const layDanhSachPhimAction = (tukhoa="") => {
  return async (dispatch) => {
    try {
      dispatch({ type: SHOW_LOADING });
      const { data } = await quanLyPhimService.layDanhSachPhim(tukhoa);
      dispatch({ type: LIST_PHIM, listPhim: data.content });
      dispatch({ type: HIDE_LOADING });
    } catch (error) {
      dispatch({ type: HIDE_LOADING });
      console.log(error.response?.data);
    }
  };
};

export const layThongTinLichChieuPhimAction = (idPhim) => {
  return async (dispatch) => {
    try {
      const { data } = await quanLyPhimService.layThongTinLichChieuPhim(idPhim);
      // console.log(data);
      dispatch({ type: SET_MOVIE_DETAIL, movieDetail: data.content });
    } catch (error) {
      console.log(error.response?.data);
    }
  };
};

export const themPhimUploadHinhAction = (filedata, resetForm) => {
  return async (dispatch) => {
    try {
       await quanLyPhimService.themPhimUploadHinh(filedata);
      // console.log(data);
      dispatch(layDanhSachPhimAction());
      openNotification("success", "Add film success!");
      resetForm();
    } catch (error) {
      openNotification("error", `${error.response?.data.content}`);
      console.log(error.response?.data);
    }
  };
};

export const layThongTinPhimAction = (idPhim) => {
  return async (dispatch) => {
    try {
      const { data } = await quanLyPhimService.layThongTinPhim(idPhim);
      // console.log(data);

      dispatch({
        type: INFO_PHIM,
        infoPhim: data.content,
      });
    } catch (error) {
      console.log(error.response?.data);
    }
  };
};
export const capNhatPhimUpdateAction = (formdata) => {
  return async (dispatch) => {
    try {
      const { data } = await quanLyPhimService.capNhatPhimUpload(formdata);
      console.log(data);
      openNotification("success", "Change film success!");
      dispatch(layDanhSachPhimAction());
    } catch (error) {
      openNotification("error", `${error.response?.data.content}`);
      console.log(error.response?.data);
    }
  };
};
export const xoaPhimAction = (idPhim) => {
  return async (dispatch) => {
    try {
      const { data } = await quanLyPhimService.xoaPhim(idPhim);
      console.log(data);
      openNotification("success", "Delete film success!");
      dispatch(layDanhSachPhimAction());
    } catch (error) {
      openNotification("error", `${error.response?.data.content}`);
      console.log(error.response?.data);
    }
  };
};

