import { quanLyDatVeSevice } from "services/QuanLyDatVe";
import { USER_LOGIN } from "settings/config";
import { HIDE_LOADING, SHOW_LOADING } from "store/types/loaderTypes";
import { DAT_VE_SUCCES, SET_PHONG_VE } from "store/types/quanLyDatVeTypes";
import {  CHUYEN_TAB } from "pages/client/checkOut/modules/types";
import { ThongTinDatVe } from "_core/models/ThongTinDatVe";
import { openNotification } from "utils/Notification";

export const layDanhSachPhongVeAction = (maLichChieu) => {
  return async (dispatch) => {
    if (localStorage.getItem(USER_LOGIN)) {
      try {
        dispatch({ type: SHOW_LOADING });
        const { data } = await quanLyDatVeSevice.layDanhSachPhongVe(
          maLichChieu
        );
        console.log(data);
        dispatch({
          type: SET_PHONG_VE,
          infoPhongVe: data.content,
        });
        dispatch({ type: HIDE_LOADING });
      } catch (error) {
        dispatch({ type: HIDE_LOADING });
        console.log(error.response?.data);
      }
    }
  };
};

export const datVeAction = (thongTinVe = new ThongTinDatVe()) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SHOW_LOADING });
      const { data } = await quanLyDatVeSevice.datVe(thongTinVe);
      console.log(data);
      await dispatch(layDanhSachPhongVeAction(thongTinVe.maLichChieu));
      await dispatch({
        type: DAT_VE_SUCCES,
      });
      await dispatch({ type: HIDE_LOADING });
      await dispatch({
        type: CHUYEN_TAB,
      });
    } catch (error) {
      dispatch({ type: HIDE_LOADING });
      console.log(error.response?.data);
    }
  };
};

export const taoLichChieuAction = (thongTinLichChieu) => {
  return async (dispatch) => {
    try {
      console.log(thongTinLichChieu);
      await quanLyDatVeSevice.taoLichChieu(thongTinLichChieu);
      openNotification("success", "Create showtime success!");
    } catch (error) {
      openNotification("error", `${error.response?.data.content}`);
      console.log(error.response?.data);
    }
  };
};
