import { TOKEN, USER_LOGIN } from "settings/config";
import {
  DANG_NHAP,
  LIST_LOAI_NGUOI_DUNG,
  SET_LIST_USERS,
  THONG_TIN_NGUOI_DUNG,
} from "store/types/quanLyNguoiDungTypes";

let user = {};
if (localStorage.getItem(USER_LOGIN)) {
  user = JSON.parse(localStorage.getItem(USER_LOGIN));
}
const initialState = {
  userLogin: user,
  thongTinNguoiDung: [],
  listUsers: [],
  listLoaiNguoiDung: [],
};

export const quanLyNguoiDungReducer = (state = initialState, action) => {
  switch (action.type) {
    case DANG_NHAP:
      localStorage.setItem(USER_LOGIN, JSON.stringify(action.thongTinDangNhap));
      localStorage.setItem(TOKEN, action.thongTinDangNhap.accessToken);
      return { ...state, userLogin: action.thongTinDangNhap };
    case THONG_TIN_NGUOI_DUNG: {
      return { ...state, thongTinNguoiDung: action.thongTinNguoiDung };
    }
    case SET_LIST_USERS: {
      state.listUsers = action.listUsers;
      return { ...state, listUser: action.listUsers };
    }
    case LIST_LOAI_NGUOI_DUNG: {
      state.listLoaiNguoiDung = action.listLoaiNguoiDung;
      return { ...state, listLoaiNguoiDung: action.listLoaiNguoiDung };
    }
    default:
      return state;
  }
};
