import { LIST_CUM_RAP, LIST_RAP } from "store/types/quanLyRapType"

const initialState = {
    listRap:[],
    listCumRap:[]
}

export const quanLyRapReducer = (state = initialState, action) => {
  switch (action.type) {

  case LIST_RAP:
    return { ...state, listRap:action.listRap }
  case LIST_CUM_RAP:
    return { ...state, listCumRap:action.listCumRap }

  default:
    return state
  }
}
