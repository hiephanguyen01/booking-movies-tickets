import { SET_PHONG_VE } from "store/types/quanLyDatVeTypes";
import { INFO_PHIM, LIST_BANNER, LIST_PHIM, SET_MOVIE_DETAIL } from "store/types/quanLyPhimTypes";

const initialState = {
  listBanner: [],
  listPhim: [],
  movieDetail: [],
  //admin
  infoPhim:[]
  
};

export const quanLyPhimReducer = (state = initialState, action) => {
  switch (action.type) {
    case LIST_BANNER:
      return { ...state, listBanner: action.listBanner };
    case LIST_PHIM: {
      return { ...state, listPhim: action.listPhim };
    }
    case SET_MOVIE_DETAIL: {
      return { ...state, movieDetail: action.movieDetail };
    }
    
    case INFO_PHIM: {
      return { ...state, infoPhim: action.infoPhim };
    }
    
    default:
      return state;
  }
};
