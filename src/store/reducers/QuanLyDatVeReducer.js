import { DAT_VE_SUCCES, SELECT_SEAT, SET_PHONG_VE } from "store/types/quanLyDatVeTypes"

const initialState = {
    infoPhongVe:[],
    danhSachGheDangDat:[],
}

export const quanLyDatVeReducer = (state = initialState, action) => {
  switch (action.type) {

  case SET_PHONG_VE:
    return { ...state,infoPhongVe:action.infoPhongVe}
  case SELECT_SEAT:
     //Cập nhật danh sách ghế đang đặt
     let danhSachGheCapNhat = [...state.danhSachGheDangDat];

     let index = danhSachGheCapNhat.findIndex(
       (gheDD) => gheDD.maGhe === action.gheDuocChon.maGhe
     );
     if (index != -1) {
       //Nếu tìm thấy ghế được chọn trong mảng có nghĩa là trước đó đã click vào rồi => xoá đi
       danhSachGheCapNhat.splice(index, 1);
     } else {
       danhSachGheCapNhat.push(action.gheDuocChon);
     }
     
     return { ...state, danhSachGheDangDat: danhSachGheCapNhat };
   
     case DAT_VE_SUCCES:{
       
       return {...state,danhSachGheDangDat:[]}
     }

  default:
    return state
  }
}
