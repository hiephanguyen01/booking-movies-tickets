import { HIDE_LOADING, SHOW_LOADING } from "store/types/loaderTypes"

const initialState = {
    loader:false
}

export const loaderReducer= (state = initialState, action) => {
  switch (action.type) {

  case HIDE_LOADING:
    return { ...state, loader: false}
  case SHOW_LOADING:
    return { ...state, loader: true}

  default:
    return state
  }
}
