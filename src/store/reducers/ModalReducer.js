import { HIDE_MODAL, SHOW_MODAL } from "store/types/modalTypes";

const initialState = {
  visible: false,
  Component: <p>fds</p>,
  playing: true,
};

export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_MODAL:
      return {
        ...state,
        playing: true,
        visible: true,
        Component: action.Component,
      };
    case HIDE_MODAL:
      return { ...state, visible: false, playing: false };

    default:
      return state;
  }
};
