import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import store from "store";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import App from "./App";
// import "./main.scss";
import "./index.css";
import "@material-tailwind/react/tailwind.css";
import "antd/dist/antd.css";
import "swiper/css";
import { GlobalStyles } from "components/GlobalStytes";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <GlobalStyles>
        <App />
      </GlobalStyles>
    </Provider>
  </React.StrictMode>
);
