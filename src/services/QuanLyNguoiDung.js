import { GROUPID } from "settings/config";

const { baseService } = require("./baseService");

class QuanLyNguoiDungSevice extends baseService {
  constructor() {
    super();
  }

  dangNhap = (info) => {
    // info:{
    //     "taiKhoan": "string",
    //     "matKhau": "string"
    //   }
    return this.post(`/api/QuanLyNguoiDung/DangNhap`, info);
  };
  dangKy = (info) => {
    // {
    //     "taiKhoan": "string",
    //     "matKhau": "string",
    //     "email": "string",
    //     "soDt": "string",
    //     "maNhom": "string",
    //     "hoTen": "string"
    //   }
    return this.post(`/api/QuanLyNguoiDung/DangKy`, info);
  };
  thongTinTaiKhoan = () => {
    return this.post(`/api/QuanLyNguoiDung/ThongTinTaiKhoan`);
  };
  layDanhSachNguoiDung = (tuKhoa = "") => {
    if (tuKhoa.trim() != "") {
      return this.get(
        `/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=${GROUPID}&tuKhoa=${tuKhoa}`
      );
    }
    return this.get(
      `/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=${GROUPID}`
    );
  };
  layDanhSachLoaiNguoiDung = () => {
    return this.get(`/api/QuanLyNguoiDung/LayDanhSachLoaiNguoiDung`);
  };
  themNguoiDung = (info) => {
    return this.post(`/api/QuanLyNguoiDung/ThemNguoiDung`, info);
  };
  capNhatThongTinNguoiDung = (user) => {
    return this.post(`/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`, user);
  };
  deleteNguoiDung = (taiKhoan) => {
    return this.delete(
      `/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`
    );
  };
  timKiemNguoiDung = (ten) => {
    return this.get(
      `/api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=GP01&tuKhoa=${ten}`
    );
  };
}

export const quanLyNguoiDungSevice = new QuanLyNguoiDungSevice();
