const { baseService } = require("./baseService");

class QuanLyRapService extends baseService{
    constructor(){
        super();
    }
    layThongTinHeThongRap = (mathethongrap="")=>{
        if(mathethongrap!=""){
            return this.get(`/api/QuanLyRap/LayThongTinHeThongRap?maHeThongRap=${mathethongrap}`)
        }
        return this.get(`/api/QuanLyRap/LayThongTinHeThongRap?maHeThongRap`)

    }
    layThongTinCumRapTheoHeThong=(maHTR)=>{
        return this.get(`/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=${maHTR}`)
    }
}
export const quanLyRapService =new QuanLyRapService();