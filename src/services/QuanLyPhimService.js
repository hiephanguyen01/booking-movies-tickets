import { GROUPID } from "settings/config";

const { baseService } = require("./baseService");

class QuanLyPhimService extends baseService {
  constructor() {
    super();
  }

  layDanhSachBanner = () => {
    return this.get(`/api/QuanLyPhim/LayDanhSachBanner`);
  };

  // layDanhSachPhim = () => {
  //   return this.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUPID}`);
  // };

  layDanhSachPhim = (tuKhoa = "") => {
    if (tuKhoa.trim() != "") {
      return this.get(
        `/api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUPID}&tenPhim=${tuKhoa}`
      );
    }
    return this.get(
      `/api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUPID}`
    );
  };
  layThongTinPhim = (idPhim) => {
    return this.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${idPhim}`);
  };
  layThongTinLichChieuPhim = (idPhim) => {
    return this.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${idPhim}`);
  };
  themPhimUploadHinh = (formdata) => {
    return this.post(`/api/QuanLyPhim/ThemPhimUploadHinh`, formdata);
  };
  capNhatPhimUpload = (formData) => {
    return this.post(`/api/QuanLyPhim/CapNhatPhimUpload`, formData);
  };

  xoaPhim = (maPhim) => {
    return this.delete(`/api/QuanLyPhim/XoaPhim?MaPhim=${maPhim}`);
  };
}

export const quanLyPhimService = new QuanLyPhimService();
