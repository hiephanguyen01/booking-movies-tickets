import React, { useEffect, memo } from "react";
import { Banner } from "components/banner/Banner";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { layDanhSachPhimAction } from "store/actions/QuanLyPhimAction";
import { Tabs } from "antd";
import "./home.scss";
import { ArrowRightOutlined } from "@ant-design/icons";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import Film from "components/card/film/Film";
import { useNavigate } from "react-router";
const { TabPane } = Tabs;
const Home = () => {
  const navigate = useNavigate();
  const { listPhim } = useSelector((state) => state.quanLyPhimReducer);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(layDanhSachPhimAction());
  },[dispatch]);

  const renderPhimDangChieu = (boolean) => (
    <div className="box-container">
      {listPhim
        ?.filter((film) => film.dangChieu === boolean)
        .slice(0, 8)
        .map((phim, idx) => {
          return <Film phim={phim} key={idx} />;
        })}
    </div>
  );
  return (
    <>
      <div className="home">
        <Banner />
        <section style={{ marginTop: "3rem" }} className="tab-movies">
          <Tabs className="tabs" defaultActiveKey="1">
            <TabPane tab="NOW SHOWING" key="1">
              {renderPhimDangChieu(true)}
              <div onClick={() => navigate("/movies")} className="loadMore">
                <span>Load more</span>
                <ArrowRightOutlined className="icon" />
              </div>
            </TabPane>
            <TabPane tab="COMMING SOON" key="2">
              {renderPhimDangChieu(false)}
              <div onClick={() => navigate("/movies")} className="loadMore">
                <span>Load more</span>
                <ArrowRightOutlined className="icon" />
              </div>
            </TabPane>
          </Tabs>
        </section>
      </div>
      {/* <Footer /> */}
      {/* <Outlet /> */}
    </>
  );
};
export default memo(Home);
