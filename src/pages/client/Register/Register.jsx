import React from "react";
import "./register.scss";
import { useFormik } from "formik";
import { useDispatch } from "react-redux";
import {
  dangKyAction,
} from "store/actions/QuanLyNguoiDungAction";
import { Navigate, useNavigate } from "react-router";
import { Link } from "react-router-dom";
import { USER_LOGIN } from "settings/config";
import * as Yup from "yup";
import { ExclamationCircleOutlined } from "@ant-design/icons";

export const Register = () => {
  const dispatch = useDispatch();
  const navigation = useNavigate();
  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDt: "",
      hoTen: "",
    },
    validationSchema: Yup.object({
      hoTen: Yup.string()
        .min(2, "Mininum 2 characters")
        .max(15, "Maximum 15 characters")
        .required("Please enter a valid name!"),
      taiKhoan: Yup.string()
        .min(2, "Mininum 2 characters")
        .max(15, "Maximum 15 characters")
        .required("Please enter a valid username!"),
      soDt: Yup.string()
        .matches(
          /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
          "Phone number is not valid"
        )
        .required("Please enter a valid phone number!"),
      email: Yup.string()
        .email("Invalid email format")
        .required("Please enter a valid email!"),
      matKhau: Yup.string()
        .min(6, "Minimum 6 characters")
        .required("Please enter a valid password!"),
      ConfirmMatKhau: Yup.string()
        .oneOf([Yup.ref("matKhau")], "Password's not match")
        .required("Please enter a valid confirm password!"),
    }),
    onSubmit: (values, props) => {
      dispatch(dangKyAction(values, navigation));
    },
  });

  if (localStorage.getItem(USER_LOGIN)) {
    return <Navigate to="/" />;
  }
  return (
    <form className="formlogin" onSubmit={formik.handleSubmit}>
      <div>
        {/* <a className="logo" href="https://www.netflix.com/" target="_blank">
          <img src="https://bit.ly/2VdIFUK" />
        </a> */}
        <div className="login" onSubmit={formik.handleSubmit}>
          <h1 className="login__title">Register</h1>
          <div className="login__group">
            <input
              className="login__group__input"
              type="text"
              placeholder="Nguyen Van A"
              name="hoTen"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
            />
            {/* <label className="login__group__label">Email or phone number</label> */}
            {formik.errors.hoTen && formik.touched.hoTen && (
              <div className="error">
                <ExclamationCircleOutlined className="icon" />
                <span className="text">{formik.errors.hoTen}</span>
              </div>
            )}
          </div>
          <div className="login__group">
            <input
              className="login__group__input"
              type="tel"
              placeholder="091777****"
              name="soDt"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
            />
            {formik.errors.soDt && formik.touched.soDt && (
              <div className="error">
                <ExclamationCircleOutlined className="icon" />
                <span className="text">{formik.errors.soDt}</span>
              </div>
            )}
          </div>
          <div className="login__group">
            <input
              className="login__group__input"
              type="text"
              placeholder="username"
              name="taiKhoan"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
            />
            {formik.errors.taiKhoan && formik.touched.taiKhoan && (
              <div className="error">
                <ExclamationCircleOutlined className="icon" />
                <span className="text">{formik.errors.taiKhoan}</span>
              </div>
            )}
          </div>
          <div className="login__group">
            <input
              className="login__group__input"
              type="text"
              placeholder="Email or phone number"
              id="email"
              name="email"
              onChange={formik.handleChange}
            />
            {formik.errors.email && formik.touched.email && (
              <div className="error">
                <ExclamationCircleOutlined className="icon" />
                <span className="text">{formik.errors.email}</span>
              </div>
            )}
          </div>
          <div className="login__group">
            <input
              className="login__group__input"
              type="password"
              placeholder="Password
              "
              id="matKhau"
              name="matKhau"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
            />
            {formik.errors.matKhau && formik.touched.matKhau && (
              <div className="error">
                <ExclamationCircleOutlined className="icon" />
                <span className="text">{formik.errors.matKhau}</span>
              </div>
            )}
          </div>
          <div className="login__group">
            <input
              className="login__group__input"
              type="password"
              placeholder="Confirm Password
              "
              name="ConfirmMatKhau"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
            />
            {formik.errors.ConfirmMatKhau && formik.touched.ConfirmMatKhau && (
              <div className="error">
                <ExclamationCircleOutlined className="icon" />
                <span className="text">{formik.errors.ConfirmMatKhau}</span>
              </div>
            )}
          </div>
          <button className="login__sign-in" type="submit">
            Register
          </button>

          <div className="signup">
            Already have a account?
            <Link to="/login">Sign in now</Link>
          </div>
        </div>
      </div>
    </form>
  );
};
