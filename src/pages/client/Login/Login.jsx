import React from "react";
import "./login.scss";
import { useFormik } from "formik";
import { useDispatch } from "react-redux";
import { dangNhapAction } from "store/actions/QuanLyNguoiDungAction";
import { Navigate, useNavigate } from "react-router";
import { Link } from "react-router-dom";
import { USER_LOGIN } from "settings/config";

export const Login = () => {
  const dispatch = useDispatch();
  const navigation = useNavigate();
  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
    },
    onSubmit: (values, props) => {
      dispatch(dangNhapAction(values, navigation));
      // navigation(-1);
    },
  });

  if (localStorage.getItem(USER_LOGIN)) {
    return <Navigate to="/" />;
  }
  return (
    <form className="formlogin" onSubmit={formik.handleSubmit}>
      <div>
        {/* <a className="logo" href="https://www.netflix.com/" target="_blank">
          <img src="https://bit.ly/2VdIFUK" />
        </a> */}
        <div className="login" onSubmit={formik.handleSubmit}>
          <h1 className="login__title">Sign In</h1>
          <div className="login__group">
            <input
              className="login__group__input"
              type="text"
              placeholder="Username"
              id="taiKhoan"
              name="taiKhoan"
              onChange={formik.handleChange}
            />
            {/* <label className="login__group__label">Email or phone number</label> */}
          </div>
          <div className="login__group">
            <input
              className="login__group__input"
              type="password"
              placeholder="Password
              "
              id="matKhau"
              name="matKhau"
              onChange={formik.handleChange}
            />
            {/* <label className="login__group__label">Password</label> */}
          </div>
          <button className="login__sign-in" type="submit">
            Sign In
          </button>

          <div className="signup">
            New to account?
            <Link to="/register">Sign Up Now</Link>
          </div>
        </div>
      </div>
    </form>
  );
};
