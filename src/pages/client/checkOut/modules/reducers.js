import { CHANGE_TAB, CHUYEN_TAB } from "./types";

const initialState = {
  tabActive: "1",
};

export const tabReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_TAB:
      return { ...state, tabActive: action.tabActive };
    case CHUYEN_TAB:
      return { ...state, tabActive: "2" };

    default:
      return state;
  }
};
