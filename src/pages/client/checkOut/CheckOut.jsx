import React, { useEffect, useMemo } from "react";
import {  useNavigate, useParams } from "react-router-dom";
import "./checkout.scss";
import { Tabs} from "antd";

import {
  LeftOutlined,

  UserOutlined,

} from "@ant-design/icons";
import { useDispatch } from "react-redux";
import {
  datVeAction,
  layDanhSachPhongVeAction,
} from "store/actions/QuanLyDatVeAction";
import { useSelector } from "react-redux";
import _ from "lodash";

import { SELECT_SEAT } from "store/types/quanLyDatVeTypes";
import {  USER_LOGIN } from "settings/config";
import { CHANGE_TAB } from "./modules/types";
import { ThongTinDatVe } from "_core/models/ThongTinDatVe";
import { thongTinTaiKhoanAction } from "store/actions/QuanLyNguoiDungAction";
import { ResultBooking } from "components/ResultBooking";
import { MenuHeader } from "components/Menu/Header";
import { openNotification } from "utils/Notification";
const { TabPane } = Tabs;

export const CheckOut = () => {
  const { maLichChieu } = useParams(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { infoPhongVe, danhSachGheDangDat } = useSelector(
    (state) => state.quanLyDatVeReducer
  );
  const { userLogin } = useSelector(
    (state) => state.quanLyNguoiDungReducer
  );
  const { tabActive } = useSelector((state) => state.tabReducer);

  const {  danhSachGhe } = infoPhongVe;
  useEffect(() => {
    dispatch(layDanhSachPhongVeAction(maLichChieu));
    dispatch({
      type: CHANGE_TAB,
      tabActive: "1",
    });
  }, []);
  useEffect(() => {
    dispatch(thongTinTaiKhoanAction());
    if (!localStorage.getItem(USER_LOGIN)) {
      openNotification("error", "Bạn không có quyền truy cập vào trang này !");
      navigate("/")
    }
  }, [infoPhongVe]);
  //start left right
  const position = ["left", "right"];
  const OperationsSlot = {
    left: (
      <div
        onClick={() => {
          navigate(-1);
        }}
        className="tabs-extra-demo-div back"
      >
        <LeftOutlined className="icon" />
        <span>BACK</span>
      </div>
    ),
    right: <MenuHeader />,
  };

  const slot = useMemo(() => {
    if (position.length === 0) return null;

    return position.reduce(
      (acc, direction) => ({ ...acc, [direction]: OperationsSlot[direction] }),
      {}
    );
  }, []);
  //end left right
  const renderListSeat = () => {
    return danhSachGhe?.map((ghe, idx) => {
      let classGheDangChon = "";
      let indexGheDD = danhSachGheDangDat.findIndex(
        (gheDD) => gheDD.maGhe === ghe.maGhe
      );
      if (indexGheDD !== -1) {
        classGheDangChon = "gheDangDat";
      }
      let classGheVip = ghe.loaiGhe === "Vip" ? "gheVip" : "";
      let classGheDaDat = ghe.daDat === true ? "gheDaDat" : "";

      let classGheDaDuocDat = "";
      if (userLogin.taiKhoan === ghe.taiKhoanNguoiDat) {
        classGheDaDuocDat = "gheDaDuocDat";
      }

      return (
        <>
          <button
            key={ghe.maGhe}
            onClick={() => {
              dispatch({
                type: SELECT_SEAT,
                gheDuocChon: ghe,
              });
            }}
            disabled={ghe.daDat}
            className={`seat ${classGheVip}  ${classGheDangChon} ${classGheDaDuocDat} ${classGheDaDat} `}
          >
            {classGheDaDuocDat ? (
              <UserOutlined style={{ marginBottom: 7.5, fontWeight: "bold" }} />
            ) : (
              ghe.stt
            )}
          </button>

          {(idx + 1) % 16 === 0 ? <br /> : ""}
        </>
      );
    });
  };

  const renderSelectSeat = () => {
    return (
      <div className="selectseat">
        <div className="rows">
          <div className="rowleft">
            <div className="listseat">
              {/* <div className="screen"></div> */}
              <div className="seats">{renderListSeat()}</div>
            </div>
            <hr className="hr" />
            <div className="notice">
              <div className="box">
                <div className="loai  checked">
                  <div></div>
                  <span>checked</span>
                </div>
                <div className="loai dachon">
                  <div></div>
                  <span>Occupied</span>
                </div>
                <div className="loai thuong">
                  <div></div>
                  <span>Standard</span>
                </div>
                <div className="loai vip">
                  <div></div>
                  <span>VIP</span>
                </div>
                <div className="loai gheDaDuocDat">
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <UserOutlined
                      style={{
                        margin: "0",
                        padding: "0",
                        fontSize: ".9rem",
                        fontWeight: "bold",
                      }}
                    />
                  </div>
                  <span>Selected</span>
                </div>
              </div>
            </div>
          </div>

          <div className="rowright">
            <div className="images">
              <img src={infoPhongVe?.thongTinPhim?.hinhAnh} alt="sas" />
              <p>{infoPhongVe?.thongTinPhim?.tenPhim}</p>
            </div>
            <div className="infodetail">
              <div className="cumrap">
                <span>Cinema:</span>
                {infoPhongVe?.thongTinPhim?.tenCumRap}
              </div>
              <div className="timechieu">
                <span>Suất Chiếu:</span>
                {infoPhongVe?.thongTinPhim?.ngayChieu} -{" "}
                {infoPhongVe?.thongTinPhim?.gioChieu}
              </div>
              <div className="seat">
                <span>Ghế:</span>
                {_.sortBy(danhSachGheDangDat, ["stt"]).map((gheDD, index) => {
                  return (
                    <span
                      key={index}
                      style={{ fontWeight: "bold", color: "red" }}
                    >
                      [{gheDD.stt}]
                    </span>
                  );
                })}
              </div>
              <div className="total">
                <span>Total:</span>
                {danhSachGheDangDat
                  .reduce((tongTien, ghe, index) => {
                    return (tongTien += ghe.giaVe);
                  }, 0)
                  .toLocaleString()}{" "}
                đ
              </div>
            </div>
            <div className="button">
              <a
                onClick={() => {
                  const thongTinDatVe = new ThongTinDatVe();
                  thongTinDatVe.maLichChieu = maLichChieu;
                  thongTinDatVe.danhSachVe = danhSachGheDangDat;
                  dispatch(datVeAction(thongTinDatVe));
                }}
              >
                Book Ticket
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="checkout">
      <Tabs
        defaultActiveKey="1"
        activeKey={tabActive}
        className="tabsCheckOut"
        tabBarExtraContent={slot}
        onChange={(key) => {
          dispatch({
            type: CHANGE_TAB,
            tabActive: key.toString(),
          });
        }}
      >
        <TabPane tab="SELECT SEAT & CHECKOUT" key="1">
          {renderSelectSeat()}
        </TabPane>
        <TabPane tab="BOOKING RESULTS" key="2">
          <ResultBooking />
        </TabPane>
      </Tabs>
    </div>
  );
};
