import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import _ from "lodash";
import { Rate } from "antd";

import moment from "moment";
import { layThongTinLichChieuPhimAction } from "store/actions/QuanLyPhimAction";
import "./moviedetail.scss";
import { LichChieu } from "components/lichchieu/LichChieu";
import { PlayCircleOutlined } from "@ant-design/icons";
import { SHOW_MODAL } from "store/types/modalTypes";
import { Trailer } from "components/Trailer";

export const MovieDetail = () => {
  const { idPhim } = useParams();
  const dispatch = useDispatch();
  const { movieDetail } = useSelector((state) => state.quanLyPhimReducer);
  useEffect(() => {
    dispatch(layThongTinLichChieuPhimAction(idPhim));
    window.scrollTo(0, 0);
  }, [dispatch, idPhim]);

  return (
    <>
      <section className="movie-detail">
        <div className="movie-card">
          <div className="containers">
            <div
              className="image"
              onClick={() => {
                dispatch({
                  type: SHOW_MODAL,
                  Component: <Trailer url={movieDetail?.trailer} />,
                });
              }}
            >
              <img src={movieDetail?.hinhAnh} alt="cover" className="cover" />
              <PlayCircleOutlined className="icon" />
            </div>
            <div className="hero ">
              <div className="details">
                <div className="title1">
                  <span> {movieDetail?.tenPhim}</span>
                  <span>HOT</span>
                </div>
                <div className="title2">
                  Release date:{" "}
                  {moment(movieDetail.ngayKhoiChieu).format("DD.MM.YYYY")}
                </div>
                <div className="rate">
                  <Rate allowHalf defaultValue={2.5} className="stars" />
                  <span className="danhgia">
                    <strong style={{ color: "#eb0606" }}>
                      {movieDetail.danhGia}
                    </strong>
                    /10
                  </span>
                  <span className="likes">109 likes</span>
                </div>
              </div>{" "}
              {/* end details */}
            </div>{" "}
            {/* end hero */}
            <div className="description">
              <div className="column2">
                <h1 className="titlecontent">MOVIE CONTENT</h1>
                <div className="content">
                  {_.size(movieDetail?.moTa) > 400 ? (
                    <>
                      <span>{movieDetail?.moTa.slice(0, 230)}. . . </span>
                      <a style={{ color: "#de0611" }} >
                        Read more
                      </a>
                    </>
                  ) : (
                    <p>{movieDetail?.moTa}</p>
                  )}
                </div>
              </div>
              {/* end column2 */}
            </div>
          </div>
        </div>

        <LichChieu heThongRapChieu={movieDetail.heThongRapChieu} />
      </section>
    </>
  );
};
