import React, { useEffect, useState } from "react";
import "./Movies.scss";
import { Select, Pagination } from "antd";
import { useDispatch, useSelector } from "react-redux";
import Film from "components/card/film/Film";
import { layDanhSachPhimAction } from "store/actions/QuanLyPhimAction";

const { Option } = Select;
const pageSize = 12;
export const Movies = () => {
  const [state, setState] = useState({
    data: [],
    totalPage: 0,
    current: 1,
    minIndex: 0,
    maxIndex: 0,
  });
  const [filter, setFilter] = useState(1);
  const { data, current, minIndex, maxIndex } = state;
  const { listPhim } = useSelector((state) => state.quanLyPhimReducer);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(layDanhSachPhimAction());
    window.scrollTo(0, 0);
  }, []);
  useEffect(() => {
    setState({
      data: listPhim,
      totalPage: listPhim.length / pageSize,
      minIndex: 0,
      maxIndex: pageSize,
    });
  }, [listPhim]);
 

  const handleChange = (page) => {
    console.log(page);
    setState({
      ...state,
      current: page,
      minIndex: (page - 1) * pageSize,
      maxIndex: page * pageSize,
    });
  };
  const handleChangeFilter = (value) => {
    if (value === 1) {
      setFilter(1);
    }
    if (value === 2) {
      setFilter(2);
    }
    if (value === 3) {
      setFilter(3);
    }
  };
  const renderPhimDangChieu = (boolean) => (
    <div className="box-container">
      {listPhim
        ?.filter((film) => film.dangChieu === boolean)
        .map((phim, index) => {
          return (
            index >= minIndex &&
            index < maxIndex && <Film phim={phim} key={index} />
          );
        })}
    </div>
  );
  return (
    <div className="movies">
      <div className="filter">
        <div className="sortBy">
          <span>Sort By:</span>
          <Select
            className="selectSort"
            defaultValue={1}
            style={{ width: 120 }}
            onChange={handleChangeFilter}
          >
            <Option value={1}>All</Option>
            <Option value={2}>Now Showing</Option>
            <Option value={3}>Comming</Option>
          </Select>
        </div>
      </div>
      <div className="box-container">
        {filter === 1 &&
          listPhim.map((phim, index) => {
            return (
              index >= minIndex &&
              index < maxIndex && <Film phim={phim} key={index} />
            );
          })}
        {filter === 2 && renderPhimDangChieu(true)}
        {filter === 3 && renderPhimDangChieu(false)}
        {/* {listPhim.map((phim, index) => {
          return (
            index >= minIndex &&
            index < maxIndex && <Film phim={phim} key={index} />
          );
        })} */}
      </div>
      <Pagination
        pageSize={pageSize}
        current={current}
        total={data.length}
        onChange={handleChange}
        style={{ marginTop: "5rem", textAlign: "center" }}
      />
    </div>
  );
};
