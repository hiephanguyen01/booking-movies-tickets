import React, { useEffect } from "react";
import { Select } from "antd";
import { useFormik } from "formik";
import "./new.scss";
import { GROUPID } from "settings/config";
import { useDispatch, useSelector } from "react-redux";
import {
  layDanhSachLoaiNguoiDungAction,
  themNguoiDungAction,
} from "store/actions/QuanLyNguoiDungAction";

export default function New(props) {
  const { listLoaiNguoiDung } = useSelector(
    (state) => state.quanLyNguoiDungReducer
  );
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(layDanhSachLoaiNguoiDungAction());
  }, []);
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDt: "",
      maNhom: GROUPID,
      maLoaiNguoiDung: listLoaiNguoiDung[0]?.maLoaiNguoiDung,
      hoTen: "",
    },
    onSubmit: (values, { resetForm }) => {
      console.log(values);
      dispatch(themNguoiDungAction(values,resetForm));
    },
  });

  const handleChange = (value) => {
    formik.setFieldValue("maLoaiNguoiDung", value);
  };
  const handleReset = (resetForm) => {
    if (window.confirm("Reset?")) {
      resetForm();
    }
  };
  return (
    <div className="new">
      <div className="newContainer">
        <div className="top">
          <h1>Add New User</h1>
        </div>
        <div className="bottom">
          <form onSubmit={formik.handleSubmit}>
            <div className="box">
              <div className="formInput">
                <label>Username</label>
                <input
                  type="text"
                  name="taiKhoan"
                  onChange={formik.handleChange}
                  value={formik.values.taiKhoan}
                  placeholder="john_doe"
                />
              </div>
              <div className="formInput">
                <label>Name and surname</label>
                <input
                  type="text"
                  name="hoTen"
                  onChange={formik.handleChange}
                  value={formik.values.hoTen}
                  placeholder="John Doe"
                />
              </div>
              <div className="formInput">
                <label>Email</label>
                <input
                  type="email"
                  name="email"
                  onChange={formik.handleChange}
                  value={formik.values.email}
                  placeholder="john_doe@gmail.com"
                />
              </div>
              <div className="formInput">
                <label>Password</label>
                <input
                  name="matKhau"
                  onChange={formik.handleChange}
                  value={formik.values.matKhau}
                  type="password"
                />
              </div>
              <div className="formInput">
                <label>Phone</label>
                <input
                  type="text"
                  name="soDt"
                  onChange={formik.handleChange}
                  value={formik.values.soDt}
                  placeholder="+84 123 12343"
                />
              </div>
              <div className="formInput">
                <label>User Type</label>
                <Select
                  className="selectuser"
                  onChange={handleChange}
                  options={listLoaiNguoiDung?.map((loai, idx) => ({
                    label: loai.tenLoai,
                    value: loai.maLoaiNguoiDung,
                  }))}
                ></Select>
              </div>
            </div>
            <div className="button">
              <button type="submit">Send</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
