import React, { useEffect, useState } from "react";
import "./users.scss";
import { Table, Popconfirm, AutoComplete } from "antd";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteNguoiDungAction,
  layDanhSachNguoiDungAction,
} from "store/actions/QuanLyNguoiDungAction";
import { SHOW_MODAL } from "store/types/modalTypes";
import { EditUser } from "components/Form/EditUser/EditUser";
import { QuestionCircleOutlined, SearchOutlined } from "@ant-design/icons";
import { useDebounce } from "hooks/useDebounce";
export const Users = () => {
  const dispatch = useDispatch();
  const { listUsers } = useSelector((state) => state.quanLyNguoiDungReducer);
  const [value, setValue] = useState("");
  const [valueInput, setValueInput] = useState("");
  const debounse = useDebounce(value, 500);
  //search user
  useEffect(() => {
    dispatch(layDanhSachNguoiDungAction(debounse.trim()));
  }, [debounse]);
  useEffect(() => {
    dispatch(layDanhSachNguoiDungAction());
  }, []);

  const options = [
    {
      label: "Name",
      options: listUsers?.map((user) => {
        return {
          value: user.taiKhoan.toString(),
          label: (
            <div
              key={user.taiKhoan}
              style={{
                display: "flex",
                gap: "10px",
              }}
            >
              <span> {user.hoTen}</span>
            </div>
          ),
        };
      }),
    },
  ];
  const columns = [
    {
      title: "Name",
      dataIndex: "hoTen",
      sorter: (a, b) => {
        if (a.hoTen.toLowerCase().trim() > b.hoTen.toLowerCase().trim()) {
          return 1;
        }
        return -1;
      },
    },
    {
      title: "Username",
      dataIndex: "taiKhoan",
    },
    {
      title: "Email ",
      dataIndex: "email",
    },
    {
      title: "Phone Number",
      dataIndex: "soDt",
    },
    {
      title: "Action",
      dataIndex: "action",
      render: (text, user, idex) => {
        return (
          <div className="cellAction">
            <a
              onClick={() => {
                {
                  // console.log(user);
                  dispatch({
                    type: SHOW_MODAL,
                    Component: <EditUser user={user} />,
                  });
                }
              }}
              style={{ textDecoration: "none" }}
            >
              <div className="viewButton">Edit</div>
            </a>
            <Popconfirm
              title="Are you sure？"
              onConfirm={() => dispatch(deleteNguoiDungAction(user.taiKhoan))}
              icon={<QuestionCircleOutlined style={{ color: "red" }} />}
            >
              <div className="deleteButton">Delete</div>
            </Popconfirm>
          </div>
        );
      },
      width: "20%",
    },
  ];
  function onChange(pagination, filters, sorter, extra) {
    // console.log("params", pagination, filters, sorter, extra);
  }

  return (
    <div className="users">
      <div className="dataTable">
        <div className="dataTableTitle">
          list users
          <div></div>
          <Link to="new" className="link" style={{ textDecoration: "none" }}>
            Add new
          </Link>
        </div>
        {/* <div className="search">
          <input
            type="text"
            name="search"
            // onChange={handleChange}
            placeholder="search..."
          />
          <SearchOutlinedIcon className="icon" />
        </div> */}
        <AutoComplete
          options={options}
          style={{ width: 200 }}
          value={debounse}
          onSelect={(value, option) => {
            setValue(value);
            setValueInput(value);
          }}
          onSearch={(value) => {
            setValue(value);
          }}
        >
          <div className="search">
            <input
              onChange={(value) => {
                setValueInput(value.target.value);
              }}
              value={valueInput}
              type="text"
              placeholder="search..."
            />
            <SearchOutlined className="icon" />
          </div>
        </AutoComplete>
        <Table
          columns={columns}
          pagination={{ defaultPageSize: 8 }}
          dataSource={listUsers}
          onChange={onChange}
          rowKey={"taiKhoan"}
        />
      </div>
    </div>
  );
};
