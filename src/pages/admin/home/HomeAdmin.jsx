import { Navbar } from "components/navbar/Navbar";
import { Sidebar } from "components/sidebar/Sidebar";
import React from "react";
import { Outlet } from "react-router";
import "./homeadmin.scss";

export default function HomeAdmin() {
  return (
    <div className="homeadmin">
      <Sidebar />
      <div className="homeContainer">
        <Navbar />
        <div className="widgets">
          <Outlet />
        </div>
      </div>
    </div>
  );
}
