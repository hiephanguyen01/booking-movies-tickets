import React, { useEffect } from "react";
import "./showtime.scss";
import { useFormik } from "formik";
import { DatePicker, InputNumber } from "antd";
import { Select } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  layThongTinCumRapTheoHeThongAction,
  layThongTinHeTHongRapAction,
} from "store/actions/QuanLyRapAction";
import moment from "moment";
import { taoLichChieuAction } from "store/actions/QuanLyDatVeAction";
import { useParams } from "react-router-dom";
import { layThongTinPhimAction } from "store/actions/QuanLyPhimAction";

export const ShowTime = () => {
  const { listRap, listCumRap } = useSelector(
    (state) => state.quanLyRapReducer
  );
  const dispatch = useDispatch();
  const { idPhim } = useParams();
  const { infoPhim } = useSelector((state) => state.quanLyPhimReducer);
  useEffect(() => {
    dispatch(layThongTinPhimAction(idPhim));
  }, [idPhim]);
  useEffect(() => {
    dispatch(layThongTinHeTHongRapAction());
  }, []);
  
  const formik = useFormik({
    initialValues: {
      maPhim: idPhim,
      ngayChieuGioChieu: "",
      maRap: "",
      giaVe: "",
    },
    onSubmit: (values, { resetForm }) => {
      dispatch(taoLichChieuAction(values, resetForm));
    },
  });

  const handleChangeHTR = (value) => {
    console.log(value);
    dispatch(layThongTinCumRapTheoHeThongAction(value));
  };
  const handleChangeCR = (value) => {
    console.log("cumrap", value);
    formik.setFieldValue("maRap", value);
  };
  const convertSelectHTR = () => {
    // state.heThongRapChieu?.map((htr, index) => ({ label: htr.tenHeThongRap, value: htr.tenHeThongRap }))
    return listRap?.map((htr, index) => {
      return { label: htr.tenHeThongRap, value: htr.maHeThongRap };
    });
  };
  const onChangeDate = (values) => {
    formik.setFieldValue(
      "ngayChieuGioChieu",
      moment(values).format("DD/MM/YYYY hh:mm:ss")
    );
  };
  const onOk = (values) => {
    formik.setFieldValue(
      "ngayChieuGioChieu",
      moment(values).format("DD/MM/YYYY hh:mm:ss")
    );
    // console.log("values", moment(values).format("DD/MM/YYYY hh:mm:ss"));
  };

  const onchangeInputNumber = (values) => {
    console.log(values);
    formik.setFieldValue("giaVe", values);
  };

  return (
    <div className="showtime">
      <div className="newContainer">
        <div className="top">
          <h1>ShowTime</h1>
        </div>
        <div className="bottom">
          <div className="left">
            <img src={infoPhim.hinhAnh} alt="" />
          </div>
          <div className="right">
            <form onSubmit={formik.handleSubmit}>
              <div className="box">
                <div className="formInput">
                  <label>Hệ Thống Rạp</label>

                  <Select
                    className="selectuser"
                    placeholder="Chọn hệ thống rạp.."
                    options={convertSelectHTR()}
                    onChange={handleChangeHTR}
                  ></Select>
                </div>
                <div className="formInput">
                  <label>Cụm Rạp</label>

                  <Select
                    placeholder="Chọn cụm rạp.."
                    className="selectuser"
                    onChange={handleChangeCR}
                    options={listCumRap?.map((cumRap, idx) => ({
                      label: cumRap.tenCumRap,
                      value: cumRap.maCumRap,
                    }))}
                  ></Select>
                </div>
                <div className="formInput">
                  <label>DateTime</label>
                  <DatePicker
                    format={"DD/MM/YYYY hh:mm:ss"}
                    className="datetime"
                    showTime
                    // value={moment(formik.values.ngayKhoiChieu)}
                    onChange={onChangeDate}
                    onOk={onOk}
                  />
                </div>
                <div className="formInput">
                  <label>Price Sticky</label>
                  <InputNumber
                    className="rate"
                    controls={false}
                    placeholder="VND"
                    onChange={onchangeInputNumber}
                    value={formik.values.giaVe}
                    onKeyPress={(e) => {
                      e.which === 13 && e.preventDefault();
                    }}
                  />
                </div>
              </div>
              <div className="button">
                <button type="submit">Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
