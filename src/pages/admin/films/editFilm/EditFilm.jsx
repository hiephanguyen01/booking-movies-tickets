import { useFormik } from "formik";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { DatePicker, Switch, InputNumber } from "antd";
import {  useNavigate, useParams } from "react-router";
import { GROUPID } from "settings/config";
import DriveFolderUploadOutlinedIcon from "@mui/icons-material/DriveFolderUploadOutlined";
import {
  capNhatPhimUpdateAction,
  layThongTinPhimAction,
} from "store/actions/QuanLyPhimAction";
import "./editfilm.scss";
import moment from "moment";

export const EditFilm = (props) => {
  const [file, setFile] = useState("");
  const { idPhim } = useParams();
  const { infoPhim } = useSelector((state) => state.quanLyPhimReducer);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(layThongTinPhimAction(idPhim));
  }, [idPhim]);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      tenPhim: infoPhim.tenPhim,
      maPhim: infoPhim.maPhim,
      trailer: infoPhim.trailer,
      moTa: infoPhim.moTa,
      ngayKhoiChieu: infoPhim.ngayKhoiChieu,
      dangChieu: infoPhim.dangChieu,
      sapChieu: infoPhim.sapChieu,
      hot: infoPhim.hot,
      danhGia: infoPhim.danhGia,
      maNhom: GROUPID,
      hinhAnh: null,
    },
    onSubmit: async (values, { resetForm }) => {
      console.log(values);

      //Tạo đối tượng formdata => Đưa giá trị values từ formik vào formdata
      let formData = new FormData();
      for (let key in values) {
        if (key !== "hinhAnh") {
          formData.append(key, values[key]);
        } else {
          if (values.hinhAnh !== null) {
            formData.append("File", values.hinhAnh, values.hinhAnh.name);
          }
        }
      }

      //Cập nhật phim upload hình
      await dispatch(capNhatPhimUpdateAction(formData));
      dispatch(layThongTinPhimAction(values.maPhim));
    },
  });
  const handleChangeFile = async (e) => {
    //Lấy file ra từ e
    // console.log(e);
    let file = e.target.files[0];

    //Đem dữ liệu file lưu vào formik
    await formik.setFieldValue("hinhAnh", file);
    //Tạo đối tượng để đọc file
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (e) => {
      //   console.log(e.target.result);
      setFile(e.target.result); //Hình base 64
    };
  };

  const handleChangeSwitch = (name) => {
    return (value) => {
      console.log(value, name);
      formik.setFieldValue(name, value);
    };
  };

  const onChangeRate =(value)=> {
    formik.setFieldValue("danhGia", value);
  }
  const onChange = (value) => {
    console.log('datepickerchange',value);
    let ngayKhoiChieu = moment(value);
    console.log('datepickerchange',ngayKhoiChieu);
    formik.setFieldValue("ngayKhoiChieu", ngayKhoiChieu);
  };

  return (
    <div className="editfilm">
      <div className="newContainer">
        <div className="top">
          <h1>Add New Film</h1>
        </div>
        <div className="bottom">
          <div className="left">
            <img src={file == "" ? infoPhim.hinhAnh : file} alt="" />
          </div>
          <div className="right">
            <form onSubmit={formik.handleSubmit}>
              <div className="box">
                <div className="formInput">
                  <label htmlFor="file" style={{ cursor: "pointer" }}>
                    Image: <DriveFolderUploadOutlinedIcon className="icon" />
                  </label>
                  <input
                    type="file"
                    id="file"
                    accept="image/png, image/jpeg,image/gif,image/png"
                    onChange={handleChangeFile}
                    style={{ display: "none" }}
                  />
                </div>
                <div className="formInput">
                  <label>Movie Title</label>
                  <input
                    type="text"
                    name="tenPhim"
                    onChange={formik.handleChange}
                    value={formik.values.tenPhim}
                    onKeyPress={(e) => {
                      e.which === 13 && e.preventDefault();
                    }}
                  />
                </div>
                <div className="formInput">
                  <label>Trailer</label>
                  <input
                    type="text"
                    name="trailer"
                    onChange={formik.handleChange}
                    value={formik.values.trailer}
                    onKeyPress={(e) => {
                      e.which === 13 && e.preventDefault();
                    }}
                  />
                </div>
                <div className="formInput">
                  <label>Description</label>
                  <input
                    type="text"
                    name="moTa"
                    onChange={formik.handleChange}
                    value={formik.values.moTa}
                    onKeyPress={(e) => {
                      e.which === 13 && e.preventDefault();
                    }}
                  />
                </div>
                <div className="formInput">
                  <label>NOW SHOWING</label>
                  <Switch
                    checked={formik.values.dangChieu}
                    onChange={handleChangeSwitch("dangChieu")}
                  />
                </div>
                <div className="formInput">
                  <label>DateTime</label>
                  <DatePicker
                    format={"DD/MM/YYYY"}
                    className="datetime"
                    value={moment(formik.values.ngayKhoiChieu)}
                    onChange={onChange}
                    // onOk={onOk}
                  />
                </div>

                <div className="formInput">
                  <label>Comming Soon</label>
                  <Switch
                    checked={formik.values.sapChieu}
                    onChange={handleChangeSwitch("sapChieu")}
                  />
                </div>
                <div className="formInput">
                  <label>Rate</label>
                  <InputNumber
                    className="rate"
                    min={1}
                    max={10}
                    controls={false}
                    onChange={onChangeRate}
                    value={formik.values.danhGia}
                    onKeyPress={(e) => {
                      e.which === 13 && e.preventDefault();
                    }}
                  />
                </div>
                <div className="formInput">
                  <label>Hot</label>
                  <Switch
                    checked={formik.values.hot}
                    onChange={handleChangeSwitch("hot")}
                  />
                </div>
              </div>
              <div className="button">
                <button type="submit">Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
