import React, { useState } from "react";
import { useFormik } from "formik";
import { DatePicker, Switch, InputNumber } from "antd";

import "./newfilm.scss";
import { GROUPID } from "settings/config";
import DriveFolderUploadOutlinedIcon from "@mui/icons-material/DriveFolderUploadOutlined";
import moment from "moment";
import { themPhimUploadHinhAction } from "store/actions/QuanLyPhimAction";
import { useDispatch } from "react-redux";

const { RangePicker } = DatePicker;

export const NewFilm = () => {
  const dispatch = useDispatch();
  const [file, setFile] = useState("");

  const handleChangeFile = (e) => {
    //Lấy file ra từ e
    // console.log(e);
    let file = e.target.files[0];
    //Tạo đối tượng để đọc file
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (e) => {
      //   console.log(e.target.result);
      setFile(e.target.result); //Hình base 64
    };
    //Đem dữ liệu file lưu vào formik
    formik.setFieldValue("hinhAnh", file);
  };

  const handleChangeSwitch = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };

  function onChangeRate(value) {
    formik.setFieldValue("danhGia", value);
  }
  function onChange(value, dateString) {
    let ngayKhoiChieu = moment(value).format("DD/MM/YYYY");
    formik.setFieldValue("ngayKhoiChieu", ngayKhoiChieu);
  }

  //   function onOk(value) {
  //     console.log("onOk: ", value);
  //   }

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      tenPhim: "",
      trailer: "",
      moTa: "",
      ngayKhoiChieu: "",
      dangChieu: false,
      sapChieu: false,
      hot: false,
      danhGia: 0,
      maNhom: GROUPID,
      hinhAnh: {},
    },
    onSubmit: (values, { resetForm }) => {
      

      //Tạo đối tượng formdata => Đưa giá trị values từ formik vào formdata
      let formData = new FormData();
      for (let key in values) {
          console.log(values[key])
        if (key !== "hinhAnh") {
          formData.append(key, values[key]);
        } else {
          formData.append("File", values.hinhAnh, values.hinhAnh.name);
        }
      }
      console.log(formData)
      //Gọi api gửi các giá trị formdata về backend xử lý
      dispatch(themPhimUploadHinhAction(formData,resetForm));
      
    },
  });
  return (
    <div className="newfilm">
      <div className="newContainer">
        <div className="top">
          <h1>Add New Film</h1>
        </div>
        <div className="bottom">
          <div className="left">
            <img
              src={
                file
                  ? `${file}`
                  : "https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg"
              }
              alt=""
            />
          </div>
          <div className="right">
            <form onSubmit={formik.handleSubmit}>
              <div className="box">
                <div className="formInput">
                  <label htmlFor="file" style={{ cursor: "pointer" }}>
                    Image: <DriveFolderUploadOutlinedIcon className="icon" />
                  </label>
                  <input
                    type="file"
                    id="file"
                    accept="image/png, image/jpeg,image/gif,image/png"
                    onChange={handleChangeFile}
                    
                    style={{ display: "none" }}
                    
                  />
                </div>
                <div className="formInput">
                  <label>Movie Title</label>
                  <input
                    type="text"
                    name="tenPhim"
                    onChange={formik.handleChange}
                    value={formik.values.tenPhim}
                    onKeyPress={(e) => {
                      e.which === 13 && e.preventDefault();
                    }}
                  />
                </div>
                <div className="formInput">
                  <label>Trailer</label>
                  <input
                    type="text"
                    name="trailer"
                    onChange={formik.handleChange}
                    value={formik.values.trailer}
                    onKeyPress={(e) => {
                      e.which === 13 && e.preventDefault();
                    }}
                  />
                </div>
                <div className="formInput">
                  <label>Description</label>
                  <input
                    type="text"
                    name="moTa"
                    onChange={formik.handleChange}
                    value={formik.values.moTa}
                    onKeyPress={(e) => {
                      e.which === 13 && e.preventDefault();
                    }}
                  />
                </div>
                <div className="formInput">
                  <label>NOW SHOWING</label>
                  <Switch
                    defaultChecked={formik.values.dangChieu}
                    onChange={handleChangeSwitch("dangChieu")}
                  />
                </div>
                <div className="formInput">
                  <label>DateTime</label>
                  <DatePicker
                    format={"DD/MM/YYYY"}
                    className="datetime"
                    
                    onChange={onChange}
                    // onOk={onOk}
                  />
                </div>

                <div className="formInput">
                  <label>Comming Soon</label>
                  <Switch
                    defaultChecked={formik.values.sapChieu}
                    onChange={handleChangeSwitch("sapChieu")}
                  />
                </div>
                <div className="formInput">
                  <label>Rate</label>
                  <InputNumber
                    className="rate"
                    min={1}
                    max={10}
                    defaultValue={0}
                    controls={false}
                    onChange={onChangeRate}
                    value={formik.values.danhGia}
                    onKeyPress={(e) => {
                      e.which === 13 && e.preventDefault();
                    }}
                  />
                </div>
                <div className="formInput">
                  <label>Hot</label>
                  <Switch
                    defaultChecked={formik.values.hot}
                    onChange={handleChangeSwitch("hot")}
                  />
                </div>
              </div>
              <div className="button">
                <button type="submit">Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
