import React, { useEffect, useState } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  layDanhSachPhimAction,
  xoaPhimAction,
} from "store/actions/QuanLyPhimAction";
import "./films.scss";
import { Link } from "react-router-dom";
import { Popconfirm, AutoComplete } from "antd";
import { QuestionCircleOutlined, SearchOutlined } from "@ant-design/icons";
import { useDebounce } from "hooks/useDebounce";

export const Films = () => {
  const dispatch = useDispatch();
  const { listPhim } = useSelector((state) => state.quanLyPhimReducer);
  const [value, setValue] = useState("");
  const [valueInput, setValueInput] = useState("");
  const debounse = useDebounce(value, 500);
  useEffect(() => {
    dispatch(layDanhSachPhimAction(debounse.trim()));
  }, [debounse]);
  useEffect(() => {
    dispatch(layDanhSachPhimAction());
  }, []);

  const options = [
    {
      label: "Name",
      options: listPhim?.map((film) => {
        return {
          value: film.tenPhim.toString(),
          label: (
            <div
              key={film.maPhim}
              style={{
                display: "flex",
                gap: "10px",
              }}
            >
              <span> {film.tenPhim}</span>
            </div>
          ),
        };
      }),
    },
  ];
  const columns = [
    // {
    //   title: "Name",
    //   dataIndex: "hoTen",
    //   sorter: (a, b) => {
    //     if (a.hoTen.toLowerCase().trim() > b.hoTen.toLowerCase().trim()) {
    //       return 1;
    //     }
    //     return -1;
    //   },
    // },
    {
      title: "ID",
      dataIndex: "maPhim",
      sorter: (a, b) => a.maPhim - b.maPhim,
      with: "10%",
    },
    {
      title: "Image",
      dataIndex: "hinhAnh",
      render: (text, record, index) => {
        return (
          <>
            <img
              src={record.hinhAnh}
              style={{
                // borderRadius: "50%",
                objectFit: "cover",
                width: "9rem",
                height: "9rem",
              }}
              alt={record.biDanh}
            />
          </>
        );
      },
      with: "15%",
    },
    {
      title: "Movie Title ",
      dataIndex: "tenPhim",
      sorter: (a, b) => {
        if (a.tenPhim.toLowerCase().trim() > b.tenPhim.toLowerCase().trim()) {
          return 1;
        }
        return -1;
      },
      width: "20%",
    },
    {
      title: "Movie Content ",
      dataIndex: "moTa",
      render: (text, record, idx) => {
        return (
          <>
            {record.moTa.length > 200 ? (
              <p>{record.moTa.slice(0, 100)}...</p>
            ) : (
              <p>{record.moTa}</p>
            )}
          </>
        );
      },
      width: "35%",
    },

    {
      title: "Action",
      dataIndex: "action",
      render: (text, record, idex) => {
        return (
          <div className="cellAction">
            <Link
              to={`edit/${record.maPhim}`}
              style={{ textDecoration: "none" }}
              onClick={() => {
                localStorage.setItem("filmEdit", JSON.stringify(record));
              }}
            >
              <div className="viewButton">Edit</div>
            </Link>
            <Popconfirm
              onConfirm={() => {
                dispatch(xoaPhimAction(record.maPhim));
              }}
              title="Are you sure？"
              icon={<QuestionCircleOutlined style={{ color: "red" }} />}
            >
              <div className="deleteButton">Delete</div>
            </Popconfirm>

            <Link
              to={`showtime/${record.maPhim}`}
              className="showTimeButton"
              onClick={() => {
                localStorage.setItem("film", JSON.stringify(record));
              }}
            >
              ShowTime
            </Link>
          </div>
        );
      },
      width: "25%",
    },
  ];
  function onChange(pagination, filters, sorter, extra) {
    // console.log("params", pagination, filters, sorter, extra);
  }

  return (
    <div className="films">
      <div className="dataTable">
        <div className="dataTableTitle">
          list films
          <Link to="new" className="link" style={{ textDecoration: "none" }}>
            Add new
          </Link>
        </div>
        <AutoComplete
          options={options}
          style={{ width: 200 }}
          value={debounse}
          onSelect={(value, option) => {
            setValue(value);
            setValueInput(value);
          }}
          onSearch={(value) => {
            setValue(value);
          }}
        >
          <div className="search">
            <input
              onChange={(value) => {
                setValueInput(value.target.value);
              }}
              value={valueInput}
              type="text"
              placeholder="search..."
            />
            <SearchOutlined className="icon" />
          </div>
        </AutoComplete>
        <Table
          columns={columns}
          pagination={{ defaultPageSize: 4 }}
          dataSource={listPhim}
          onChange={onChange}
          rowKey={"maPhim"}
        />
      </div>
    </div>
  );
};
